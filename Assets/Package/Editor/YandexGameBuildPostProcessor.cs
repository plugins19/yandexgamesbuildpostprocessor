#if UNITY_EDITOR && UNITY_WEBGL

using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;

namespace AttaboysGames.YandexGameBuildPostProcessor.Editor.Package.Editor
{
    public static partial class YandexGameBuildPostProcessor 
    {
        private const string INDEX_HTML_FILE_NAME = "index.html";

        [PostProcessBuild(0)]
        public static void CorrectFile(BuildTarget target, string pathToBuiltProject)
        {
            var desiredScript = Path.Combine(pathToBuiltProject, INDEX_HTML_FILE_NAME);
            File.WriteAllText(desiredScript, _jsCode);
        }
    }
}
#endif
