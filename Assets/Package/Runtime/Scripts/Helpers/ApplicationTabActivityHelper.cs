using AttaboysGames.YandexGameBuildPostProcessor.Runtime.Package.Runtime.Scripts.Storages;
using UnityEngine;
using Zenject;

namespace AttaboysGames.YandexGameBuildPostProcessor.Runtime.Package.Runtime.Scripts.Helpers
{
	public class ApplicationTabActivityHelper : MonoBehaviour
	{
		[Inject] private IApplicationTabActivityStorage _applicationTabActivityStorage;
		
		public void SetState(string state)
		{
			switch (state)
			{
				case "true":
					_applicationTabActivityStorage.SetState(true);
					break;
				case "false":
					_applicationTabActivityStorage.SetState(false);
					break;
			}
		}
	}
}
