using Attaboys.PlatformType.Package.Runtime.Db;
using AttaboysGames.YandexGameBuildPostProcessor.Runtime.Package.Runtime.Scripts.Helpers;
using AttaboysGames.YandexGameBuildPostProcessor.Runtime.Package.Runtime.Scripts.Storages.Impls;
using UnityEngine;
using Zenject;

namespace AttaboysGames.YandexGameBuildPostProcessor.Runtime.Package.Runtime.Scripts.Installers
{
	[CreateAssetMenu(menuName = "Installers/Attaboys/" + nameof(ApplicationTabActiveInstaller),
		fileName = nameof(ApplicationTabActiveInstaller))]
	public class ApplicationTabActiveInstaller : ScriptableObjectInstaller
	{
		[Inject] private IPlatformTypeDatabase _platformTypeDatabase;

		public override void InstallBindings()
		{
			Container.BindInterfacesTo<ApplicationTabActivityStorage>().AsSingle();

			if(_platformTypeDatabase.GetPlatform() != EPlatformType.WebGl)
				return;

			var helper = Container.InstantiateComponentOnNewGameObject<ApplicationTabActivityHelper>();
			Container.Bind<ApplicationTabActivityHelper>().FromInstance(helper).AsSingle();
		}
	}
}