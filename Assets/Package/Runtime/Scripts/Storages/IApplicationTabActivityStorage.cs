using UniRx;

namespace AttaboysGames.YandexGameBuildPostProcessor.Runtime.Package.Runtime.Scripts.Storages
{
	public interface IApplicationTabActivityStorage
	{
		IReactiveProperty<bool> IsTabActive { get; }

		void SetState(bool state);
	}
}