using UniRx;

namespace AttaboysGames.YandexGameBuildPostProcessor.Runtime.Package.Runtime.Scripts.Storages.Impls
{
	public class ApplicationTabActivityStorage : IApplicationTabActivityStorage
	{
		private readonly BoolReactiveProperty _isTabActive = new(true);
		public IReactiveProperty<bool> IsTabActive => _isTabActive;
		
		public void SetState(bool state) => _isTabActive.Value = state;
	}
}